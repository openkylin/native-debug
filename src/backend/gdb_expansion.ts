import { MINode } from "./mi_parse";

const resultRegex = /^([a-zA-Z_\-][a-zA-Z0-9_\-<> :(),]*|\[\d+\])\s*=\s*/;
const variableRegex = /^[a-zA-Z_\-\'\(][a-zA-Z0-9_\-\>\ \\\'\)\:]*/;
const errorRegex = /^\<.+?\>/;
const referenceStringRegex = /^(0x[0-9a-fA-F]+\s*)"/;
const referenceRegex = /^(0x[0-9a-fA-F]+)/;
const referenceRegexQt = /^(0x[0-9a-fA-F]+)([\<a-zA-Z0-9\,\ \:\*\)\_\(]*)([\>])/;
const cppReferenceRegex = /^@0x[0-9a-fA-F]+/;
const nullpointerRegex = /^0x0+\b/;
const charRegex = /^(\d+) ['"]/;
const numberRegex = /^(-)?\d+(\.\d+)?(e-\d+)?/;
const pointerCombineChar = ".";
const nullTupleRegex = /^(0x[0-9a-fA-F]+)}/;
import logger from './log';

export function isExpandable(value: string): number {
    let match;
    value = value.trim();
    if (value.length == 0) return 0;
    else if (value.startsWith("{...}")) return 2; // lldb string/array
    else if (value[0] == '{') return 1; // object
    else if (value.startsWith("true")) return 0;
    else if (value.startsWith("false")) return 0;
    else if (match = nullpointerRegex.exec(value)) return 0;
    else if (match = referenceStringRegex.exec(value)) return 0;
    else if (match = referenceRegex.exec(value)) return 2; // reference
    else if (match = charRegex.exec(value)) return 0;
    else if (match = numberRegex.exec(value)) return 0;
    else if (match = variableRegex.exec(value)) return 0;
    else if (match = errorRegex.exec(value)) return 0;
    else if (match = referenceRegexQt.exec(value)) return 2;
    else return 0;
}

export function expandValue(variableCreate: Function, value: string, root: string = "", extra: any = undefined): any {
    logger.debug(`expandValue1: value:${JSON.stringify(value)}`);

    let jsonVar = false;
    if (value.indexOf("=") != -1) {
        jsonVar = true;
    }
    const parseCString = () => {
        value = value.trim();
        if (value[0] != '"' && value[0] != '\'')
            return "";
        let stringEnd = 1;
        let inString = true;
        const charStr = value[0];
        let remaining = value.substring(1);
        let escaped = false;
        while (inString) {
            if (escaped)
                escaped = false;
            else if (remaining[0] == '\\')
                escaped = true;
            else if (remaining[0] == charStr)
                inString = false;

            remaining = remaining.substr(1);
            stringEnd++;
        }
        const str = value.substring(0, stringEnd).trim();
        value = value.substring(stringEnd).trim();
        return str;
    };

    const stack = [root];
    // let parseValue, parseCommaResult, parseCommaValue, parseResult, createValue;
    let parseValue: () => any, parseCommaResult: (pushToStack: boolean) => any, parseCommaValue: () => any, parseResult: (pushToStack: boolean) => any, createValue: (name: string, val: any) => any;

    let variable = "";

    const getNamespace = (variable) => {
        let namespace = "";
        let prefix = "";
        stack.push(variable);
        stack.forEach(name => {
            prefix = "";
            if (name != "") {
                if (name.startsWith("["))
                    namespace = namespace + name;
                else {
                    if (namespace) {
                        while (name.startsWith("*")) {
                            prefix += "*";
                            name = name.substring(1);
                        }
                        namespace = namespace + pointerCombineChar + name;
                    } else if (root && root.startsWith("*")) {
                        namespace = name.substring(1);
                    }
                    else {
                        namespace = name;
                    }
                }
            }
        });
        stack.pop();
        return prefix + namespace;
    };

    //解析字典，json
    const parseTupleOrList = () => {
        logger.debug(`parseTupleOrList: value:${JSON.stringify(value)}`);

        value = value.trim();
        if (value[0] != '{')
            return undefined;
        const oldContent = value;
        value = value.substring(1).trim();
        if (value[0] == '}') {
            value = value.substring(1).trim();
            return [];
        }
        if (value.startsWith("...")) {
            value = value.substring(3).trim();
            if (value[0] == '}') {
                value = value.substring(1).trim();
                return <any>"<...>";
            }
        }
        const eqPos = value.indexOf("=");
        const newValPos1 = value.indexOf("{");
        const newValPos2 = value.indexOf(",");
        const newValPos3 = value.indexOf("}")
        let newValPos = newValPos1;

        if (newValPos2 != -1 && newValPos2 < newValPos1) {
            newValPos = newValPos2;
        }

        logger.debug(`xh:parseTupleOrList:  value:${value}`);
        logger.debug(`xh:parseTupleOrList:  newValPos3:${newValPos3}, newValPos:${newValPos}, newValPos2:${newValPos2}, eqPos:${eqPos}`)
        if (newValPos != -1 && eqPos > newValPos || eqPos == -1 || eqPos > newValPos3 || value.startsWith("std::")) { // is value list
            logger.debug(`xh:parseTupleOrList:  is value list`);

            const values = [];
            stack.push("[0]");
            let val = parseValue();
            stack.pop();

            if (typeof val == "string" && val.endsWith('>')) {
                val = val.substring(0, val.length - 2);
            }

            values.push(createValue("[0]", val));
            let i = 0;
            for (; ;) {
                stack.push("[" + (++i) + "]");
                if (!(val = parseCommaValue())) {
                    stack.pop();
                    break;
                }
                stack.pop();
                values.push(createValue("[" + i + "]", val));
            }
            value = value.substring(1).trim(); // }
            logger.debug(`parseTupleOrList: values0:${JSON.stringify(values)}`);

            return values;
        }

        let result = parseResult(true);
        if (result) {
            const results = [];
            results.push(result);
            while (result = parseCommaResult(true))
                results.push(result);
            value = value.substring(1).trim(); // }
            logger.debug(`parseTupleOrList: results:${JSON.stringify(results)}`);
            return results;
        } else if (nullTupleRegex.exec(value)) {
            const match = nullTupleRegex.exec(value);
            value = value.substring(match[1].length).trim();
            const values = [];
            stack.push("[0]");
            const val = match[1];
            stack.pop();
            values.push(createValue("[0]", val));
            return values;
        }
        return undefined;
    };

    const parsePrimitive = () => {
        let primitive: any;
        let match;
        value = value.trim();
        if (value.length == 0)
            primitive = undefined;
        else if (value.startsWith("true")) {
            primitive = "true";
            value = value.substring(4).trim();
        } else if (value.startsWith("false")) {
            primitive = "false";
            value = value.substring(5).trim();
        } else if (match = nullpointerRegex.exec(value)) {
            primitive = "<nullptr>";
            value = value.substring(match[0].length).trim();
        } else if (match = referenceStringRegex.exec(value)) {
            value = value.substring(match[1].length).trim();
            primitive = parseCString();
        } else if (match = referenceRegexQt.exec(value)) {
            primitive = "*" + match[0];
            value = value.substring(match[0].length).trim();
        } else if (match = referenceRegex.exec(value)) {
            primitive = "*" + match[0];
            value = value.substring(match[0].length).trim();
        } else if (match = cppReferenceRegex.exec(value)) {
            primitive = match[0];
            value = value.substring(match[0].length).trim();
        } else if (match = charRegex.exec(value)) {
            primitive = match[1];
            value = value.substring(match[0].length - 1);
            primitive += " " + parseCString();
        } else if (match = numberRegex.exec(value)) {
            primitive = match[0];
            value = value.substring(match[0].length).trim();
        } else if (match = variableRegex.exec(value)) {
            primitive = match[0];
            value = value.substring(match[0].length).trim();
        } else if (match = errorRegex.exec(value)) {
            primitive = match[0];
            value = value.substring(match[0].length).trim();
        } else {
            primitive = value;
        }

        return primitive;
    };

    parseValue = () => {
        value = value.trim();
        // if(value[0] == ','){
        // 	value = value.substr(1).trim();
        // }

        if (value[0] == '"') {			//解析字符串
            return parseCString();
        } else if (value[0] == '{') {		//解析字典
            return parseTupleOrList();
        } else if (value.startsWith("std::")) {
            const eqPos = value.indexOf("=");
            value = value.substring(eqPos + 1).trim();
            logger.debug(`xh:parseValue: std::value:${value}`);
            return parseValue();
        } else {
            return parsePrimitive();
        }
    };

    parseResult = (pushToStack: boolean = false) => {
        logger.debug(`parseResult: value:${JSON.stringify(value)}`);
        if (value[0] == '<') {
            value = value.substring(1).trim();
        }
        value = value.trim();
        value = value.replace(/^static /, "");
        const variableMatch = resultRegex.exec(value);
        if (!variableMatch) {
            logger.debug(`parseResult: match fail, value:${JSON.stringify(value)}`);
            return undefined;
        }
        value = value.substring(variableMatch[0].length).trim();

        let name = variable = variableMatch[1].trim();
        if (name.charAt(name.length - 1) === ">") {
            name = name.slice(0, -1);
        }

        const tmpName = name.split(" ");
        if (tmpName.length > 1 && !name.includes("anonymous union") && !name.includes(',')) {
            name = tmpName[tmpName.length - 1];
        }

        //JSON.parse()
        if (pushToStack) {
            stack.push(variable);
        }
        logger.debug(`parseResult: will parseValue, name:${name}, value:${JSON.stringify(value)}`);

        const val = parseValue();
        if (pushToStack) {
            stack.pop();
        }
        return createValue(name, val);
    };

    createValue = (name, val) => {
        let ref = 0;
        let evaluateName;

        if (typeof val == "object") {
            ref = variableCreate(val);
            val = "Object";
        } else if (typeof val == "string" && val.startsWith("*0x")) {
            if (extra && MINode.valueOf(extra, "arg") == "1") {
                const isVoid = MINode.valueOf(extra, "type") === "void *";
                evaluateName = getNamespace((isVoid ? "" : "*") + name);
                ref = variableCreate(getNamespace((isVoid ? "" : "*") + "(" + name), { arg: true, isVoid });
                val = "<args>";
            } else {
                evaluateName = getNamespace("*" + name);
                ref = variableCreate(evaluateName);
                val = "Object@" + val;
            }
        } else if (typeof val == "string" && val.startsWith("@0x")) {
            evaluateName = getNamespace("*&" + name);
            ref = variableCreate(evaluateName);
            val = "Ref" + val;
        } else if (typeof val == "string" && val.startsWith("<...>")) {
            evaluateName = getNamespace(name);
            ref = variableCreate(evaluateName);
            val = "...";
        }

        value = value.trim();
        if (value[0] == ',') {
            let tmp = value
            tmp = tmp.substring(1).trim();
            tmp = tmp.trim();
            if (tmp[0] == '{' && jsonVar == true && !name.startsWith('[')) {
                logger.debug(`createValue: name:${name},value${value}`);
                tmp = ",anonymous union = " + tmp;
                value = tmp;
            } else if (tmp.startsWith("<No data fields>")) {
                value = tmp = tmp.substring("<No data fields>".length);
            }
        }

        return {
            name: name,
            value: val,
            variablesReference: ref
        };
    };

    parseCommaValue = () => {
        value = value.trim();
        if (value[0] != ',')
            return undefined;
        value = value.substring(1).trim();
        return parseValue();
    };

    parseCommaResult = (pushToStack: boolean = false) => {
        value = value.trim();
        if (value[0] != ',')
            return undefined;
        value = value.substring(1).trim();
        return parseResult(pushToStack);
    };

    value = value.trim();
    logger.debug(`expandValue: value:${JSON.stringify(value)}`);
    return parseValue();
}
