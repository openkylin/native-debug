import { DebugProtocol } from 'vscode-debugprotocol';
import * as childProcess from 'child_process';
import { EventEmitter } from 'events';
import * as stream from 'stream';
import * as fs from 'fs';
import * as readline from 'readline';
import { SSHArguments, ValuesFormattingMode } from './backend';

export interface DisassemblyInstruction {
    address: string;
    functionName: string;
    offset: number;
    instruction: string;
    opcodes: string;
}

export enum ADAPTER_DEBUG_MODE {
    NONE = 'none',
    PARSED = 'parsed',
    BOTH = 'both',
    RAW = 'raw',
    VSCODE = 'vscode'
}

export interface ElfSection {
    name: string;
    address: number;            // New base address
    addressOrig: number;        // original base address in Elf file
}
export interface SymbolFile {
    file: string;
    offset?: number;
    textaddress?: number;
    sections: ElfSection[];
    sectionMap: {[name: string]: ElfSection};
}
export interface LaunchRequestArguments extends DebugProtocol.LaunchRequestArguments {
    cwd: string;
    target: string;
    gdbpath: string;
    env: any;
    //objdumpPath:string;
    //symbolFiles: SymbolFile[];
    debugger_args: string[];
    pathSubstitutions: { [index: string]: string };
    arguments: string;
    terminal: string;
    autorun: string[];
    stopAtEntry: boolean | string;
    ssh: SSHArguments;
    valuesFormatting: ValuesFormattingMode;
    printCalls: boolean;
    showDevDebugOutput: boolean;
}

export interface AttachRequestArguments extends DebugProtocol.AttachRequestArguments {
    cwd: string;
    target: string;
    gdbpath: string;
    env: any;
    //objdumpPath:string;
    //symbolFiles: SymbolFile[];
    debugger_args: string[];
    pathSubstitutions: { [index: string]: string };
    executable: string;
    remote: boolean;
    autorun: string[];
    stopAtConnect: boolean;
    stopAtEntry: boolean | string;
    ssh: SSHArguments;
    valuesFormatting: ValuesFormattingMode;
    printCalls: boolean;
    showDevDebugOutput: boolean;
}

// Helper function to create a symbolFile object properly with required elements
export function defSymbolFile(file: string): SymbolFile {
    const ret: SymbolFile = {
        file: file,
        sections: [],
        sectionMap: {}
    };
    return ret;
}

export function hexFormat(value: number, padding: number = 8, includePrefix: boolean = true): string {
    let base = (value).toString(16);
    base = base.padStart(padding, '0');
    return includePrefix ? '0x' + base : base;
}

export interface ConfigurationArguments extends DebugProtocol.LaunchRequestArguments {
    name: string;
    request: string;
    toolchainPath: string;
    toolchainPrefix: string;
    executable: string;
    servertype: string;
    serverpath: string;
    gdbPath: string;
    objdumpPath: string;
    serverArgs: string[];
    serverCwd: string;
    device: string;
    loadFiles: string[];
    symbolFiles: SymbolFile[];
    debuggerArgs: string[];
    preLaunchCommands: string[];
    postLaunchCommands: string[];
    overrideLaunchCommands: string[];
    preAttachCommands: string[];
    postAttachCommands: string[];
    overrideAttachCommands: string[];
    preRestartCommands: string[];
    postRestartCommands: string[];
    overrideRestartCommands: string[];
    postStartSessionCommands: string[];
    postRestartSessionCommands: string[];
    overrideGDBServerStartedRegex: string;
    breakAfterReset: boolean;
    //svdFile: string;
    //svdAddrGapThreshold: number;
    //ctiOpenOCDConfig: CTIOpenOCDConfig;
    //rttConfig: RTTConfiguration;
    //swoConfig: SWOConfiguration;
    graphConfig: any[];
    /// Triple slashes will cause the line to be ignored by the options-doc.py script
    /// We don't expect the following to be in booleann form or have the value of 'none' after
    /// The config provider has done the conversion. If it exists, it means output 'something'
    showDevDebugOutput: ADAPTER_DEBUG_MODE;
    showDevDebugTimestamps: boolean;
    cwd: string;
    extensionPath: string;
    rtos: string;
    //interface: 'jtag' | 'swd' | 'cjtag';
    targetId: string | number;
    runToMain: boolean;         // Deprecated: kept here for backwards compatibility
    runToEntryPoint: string;
    registerUseNaturalFormat: boolean;
    variableUseNaturalFormat: boolean;
    //chainedConfigurations: ChainedConfigurations;

    // pvtRestartOrReset: boolean;
    // pvtPorts: { [name: string]: number; };
    // pvtParent: ConfigurationArguments;
    // pvtMyConfigFromParent: ChainedConfig;     // My configuration coming from the parent
    // pvtAvoidPorts: number[];
    // pvtVersion: string;                       // Version from package.json

    numberOfProcessors: number;
    targetProcessor: number;


    // QEMU Specific
    cpu: string;
    machine: string;

    // External
    gdbTarget: string;
}


export class HrTimer {
    private start: bigint;
    constructor() {
        this.start = process.hrtime.bigint();
    }

    public restart(): void {
        this.start = process.hrtime.bigint();
    }

    public getStart(): bigint {
        return this.start;
    }

    public deltaNs(): string {
        return (process.hrtime.bigint() - this.start).toString();
    }

    public deltaUs(): string {
        return this.toStringWithRes(3);
    }

    public deltaMs(): string {
        return this.toStringWithRes(6);
    }

    public createPaddedMs(padding: number): string {
        const hrUs = this.deltaMs().padStart(padding, '0');
        // const hrUsPadded = (hrUs.length < padding) ? '0'.repeat(padding - hrUs.length) + hrUs : '' + hrUs ;
        // return hrUsPadded;
        return hrUs;
    }

    public createDateTimestamp(): string {
        const hrUs = this.createPaddedMs(6);
        const date = new Date();
        const ret = `[${date.toISOString()}, +${hrUs}ms]`;
        return ret;
    }

    private toStringWithRes(res: number) {
        const diff = process.hrtime.bigint() - this.start + BigInt((10 ** res) / 2);
        let ret = diff.toString();
        ret = ret.length <= res ? '0' : ret.substr(0, ret.length - res);
        return ret;
    }
}

//
// For callback `cb`, fatal = false when file exists but header does not match. fatal = true means
// we could not even read the file. Use `cb` to print what ever messages you want. It is optional.
//
// Returns true if the ELF header match the elf magic number, false in all other cases
//
export function validateELFHeader(exe: string, cb?: (str: string, fatal: boolean) => void): boolean {
    try {
        if (!fs.existsSync(exe)) {
            if (cb) {
                cb(`File not found "executable": "${exe}"`, true);
            }
            return false;
        }
        const buffer = Buffer.alloc(16);
        const fd = fs.openSync(exe, 'r');
        const n = fs.readSync(fd, buffer, 0, 16, 0);
        fs.closeSync(fd);
        if (n !== 16) {
            if (cb) {
                cb(`Could not read 16 bytes from "executable": "${exe}"`, true);
            }
            return false;
        }
        // First four chars are 0x7f, 'E', 'L', 'F'
        if ((buffer[0] !== 0x7f) || (buffer[1] !== 0x45) || (buffer[2] !== 0x4c) || (buffer[3] !== 0x46)) {
            if (cb) {
                cb(`Not a valid ELF file "executable": "${exe}". Many debug functions can fail or not work properly`, false);
            }
            return false;
        }
        return true;
    }
    catch (e) {
        if (cb) {
            cb(`Could not read file "executable": "${exe}" ${e ? e.toString() : ''}`, true);
        }
        return false;
    }
}

//
// You have two choices.
// 1. Get events that you subscribe to or
// 2. get immediate callback and you will not get events
//
// There are three events
//  emit('error', err)                -- only emit
//  emit('close') and cb(null)
//  emit('line', line)  or cb(line)   -- NOT both, line can be empty ''
//  emit('exit', code, signal)        -- Only emit, NA for a stream Readable
//
// Either way, you will get a promise though. On Error though no rejection is issued and instead, it will
// emit and error and resolve to false
//
// You can chose to change the callback anytime -- perhaps based on the state of your parser. The
// callback has to return true to continue reading or false to end reading
//
// On exit for program, you only get an event. No call back.
//
// Why? Stuff like objdump/nm can produce very large output and reading them into a mongo
// string is a disaster waiting to happen. It is slow and will fail at some point. On small
// output, it may be faster but not on large ones. Tried using temp files but that was also
// slow. In this mechanism we use streams and NodeJS readline to hook things up and read
// things line at a time. Most of that kind of output needs to be parsed line at a time anyways
//
// Another benefit was we can run two programs at the same time and get the output of both in
// the same time as running just one. NodeJS is amazing juggling stuff and although not-multi threaded
// it almost look like it
//
// Finally, you can also use a file or a stream to read instead of a program to run.
//
export class SpawnLineReader extends EventEmitter {
    public callback: (line: string) => boolean;
    private promise: Promise<boolean>;
    constructor() {
        super();
    }

    public startWithProgram(
        prog: string, args: readonly string[] = [],
        spawnOpts: childProcess.SpawnOptions = {}, cb: (line: string) => boolean = undefined): Promise<boolean> {
        if (this.promise) { throw new Error('SpawnLineReader: can\'t reuse this object'); }
        this.callback = cb;
        this.promise = new Promise<boolean>((resolve) => {
            try {
                const child = childProcess.spawn(prog, args, spawnOpts);
                child.on('error', (err) => {
                    this.emit('error', err);
                    resolve(false);
                });
                child.on('exit', (code: number, signal: string) => {
                    this.emit('exit', code, signal);
                    // read-line will resolve. Not us
                });
                this.doReadline(child.stdout, resolve);
            }
            catch (e) {
                this.emit('error', e);
            }
        });
        return this.promise;
    }

    public startWithStream(rStream: stream.Readable, cb: (line: string) => boolean = undefined): Promise<boolean> {
        if (this.promise) { throw new Error('SpawnLineReader: can\'t reuse this object'); }
        this.callback = cb;
        this.promise =  new Promise<boolean>((resolve) => {
            this.doReadline(rStream, resolve);
        });
        return this.promise;
    }

    public startWithFile(filename: fs.PathLike, options: string | any = undefined, cb: (line: string, err?: any) => boolean = undefined): Promise<boolean> {
        if (this.promise) { throw new Error('SpawnLineReader: can\'t reuse this object'); }
        this.callback = cb;
        this.promise = new Promise<boolean>((resolve) => {
            const readStream = fs.createReadStream(filename, options || {flags: 'r'});
            readStream.on('error', ((e) => {
                this.emit('error', e);
                resolve(false);
            }));
            readStream.on('open', (() => {
                this.doReadline(readStream, resolve);
            }));
        });
        return this.promise;
    }

    private doReadline(rStream: stream.Readable, resolve) {
        try {
            const rl = readline.createInterface({
                input: rStream,
                crlfDelay: Infinity,
            });
            rl.on('line', (line) => {
                if (this.callback) {
                    if (!this.callback(line)) {
                        rl.close();
                    }
                } else {
                    this.emit('line', line);
                }
            });
            rl.once('close', () => {
                if (this.callback) {
                    this.callback(undefined);
                }
                rStream.destroy();
                this.emit('close');
                resolve(true);
            });
        }
        catch (e) {
            this.emit('error', e);
        }
    }
}