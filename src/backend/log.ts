import fs = require('fs');
import path = require('path');
import os = require('os');

const MAX_LOG_SIZE = 10 * 1024 * 1024; // 10 MB
const MAX_LOG_FILES = 7;

const LOG_LEVEL = {
    DEBUG: 'DEBUG',
    INFO: 'INFO',
    WARNING: 'WARNING',
    ERROR: 'ERROR'
};

const SETLOGLEVEL = LOG_LEVEL.INFO;
const localPath = `${os.homedir()}/.debug`;
fs.mkdirSync(localPath, { recursive: true });

const logFilePath = path.join(localPath, 'log_1.txt');

// 输出通道
function initializeLogFile() {
    try {
        // Check if log directory exists
        if (!fs.existsSync(localPath)) {
            fs.mkdirSync(localPath, { recursive: true });
        }

        // Get all existing log files
        const existingLogs = fs.readdirSync(localPath).filter((file: string) => file.startsWith('log_') && file.endsWith('.txt'));

        // Remove excess log files if needed
        while (existingLogs.length >= MAX_LOG_FILES) {
            const oldestLogFile = getOldestLogFile(existingLogs);
            const filePath = path.join(localPath, oldestLogFile);
            fs.unlinkSync(filePath);
            console.info(`Removed old log file: ${oldestLogFile}`);
            existingLogs.splice(existingLogs.indexOf(oldestLogFile), 1);
        }

        // Create new log file if it doesn't exist or if the current one exceeds the size limit
        if (!fs.existsSync(logFilePath) || fs.statSync(logFilePath).size > MAX_LOG_SIZE) {
            rotateLogFiles();
        }
    } catch (error) {
        console.error('Failed to initialize log file:', error);
    }
}

function getOldestLogFile(logFiles: string | any[]) {
    let oldestFile = logFiles[0];
    let oldestFileCreationTime = fs.statSync(path.join(localPath, oldestFile)).birthtime;
    for (let i = 1; i < logFiles.length; i++) {
        const file = logFiles[i];
        const fileCreationTime = fs.statSync(path.join(localPath, file)).birthtime;
        if (fileCreationTime < oldestFileCreationTime) {
            oldestFile = file;
            oldestFileCreationTime = fileCreationTime;
        }
    }
    return oldestFile;
}

function rotateLogFiles() {
    try {
        // Rename existing log files with incremented numbers
        for (let i = MAX_LOG_FILES - 1; i > 0; i--) {
            const sourceFilePath = path.join(localPath, `log_${i}.txt`);
            const destinationFilePath = path.join(localPath, `log_${i + 1}.txt`);
            if (fs.existsSync(sourceFilePath)) {
                fs.renameSync(sourceFilePath, destinationFilePath);
                console.info(`Renamed log file ${sourceFilePath} to ${destinationFilePath}`);
            }
        }

        // Create new log file
        fs.writeFileSync(logFilePath, '');
    } catch (error) {
        console.error('Failed to rotate log files:', error);
    }
}

initializeLogFile();

function log(level: string, message: any) {
    const timestamp = new Date().toISOString();
    const logMessage = `[${timestamp}] [${level}] ${message}`;
    if (getLogLevelValue(level) >= getLogLevelValue(SETLOGLEVEL)) {
        fs.appendFileSync(logFilePath, logMessage + '\n');
        if (fs.statSync(logFilePath).size > MAX_LOG_SIZE) {
            rotateLogFiles();
        }
    }
}

function debug(message: any) {
    log(LOG_LEVEL.DEBUG, message);
}

function info(message: any) {
    log(LOG_LEVEL.INFO, message);
}

function warning(message: any) {
    log(LOG_LEVEL.WARNING, message);
}

function error(message: any) {
    log(LOG_LEVEL.ERROR, message);
}

function getLogLevelValue(level: string) {
    return Object.keys(LOG_LEVEL).indexOf(level);
}

export default {
    debug,
    info,
    warning,
    error,
    LOG_LEVEL,
    SETLOGLEVEL
};