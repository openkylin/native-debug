import { MI2DebugSession, RunCommand } from './mibase';
import { DebugSession, InitializedEvent, TerminatedEvent, StoppedEvent, OutputEvent, Thread, StackFrame, Scope, Source, Handles } from 'vscode-debugadapter';
import { DebugProtocol } from 'vscode-debugprotocol';
import { MI2, escape } from "./backend/mi2/mi2";
import { SSHArguments, ValuesFormattingMode } from './backend/backend';
import { GdbDisassembler } from './backend/disasm';
import * as child_process from 'child_process';

export interface LaunchRequestArguments extends DebugProtocol.LaunchRequestArguments {
    cwd: string;
    target: string;
    gdbpath: string;
    env: any;
    debugger_args: string[];
    pathSubstitutions: { [index: string]: string };
    arguments: string;
    terminal: string;
    autorun: string[];
    stopAtEntry: boolean | string;
    ssh: SSHArguments;
    valuesFormatting: ValuesFormattingMode;
    printCalls: boolean;
    showDevDebugOutput: boolean;
}

export interface AttachRequestArguments extends DebugProtocol.AttachRequestArguments {
    cwd: string;
    target: string;
    gdbpath: string;
    env: any;
    debugger_args: string[];
    pathSubstitutions: { [index: string]: string };
    executable: string;
    remote: boolean;
    autorun: string[];
    stopAtConnect: boolean;
    stopAtEntry: boolean | string;
    ssh: SSHArguments;
    valuesFormatting: ValuesFormattingMode;
    printCalls: boolean;
    showDevDebugOutput: boolean;
}

export class GDBDebugSession extends MI2DebugSession {
    public args:LaunchRequestArguments | AttachRequestArguments;
    protected disassember: GdbDisassembler;
    protected supportsRunInTerminalRequest = false;

    protected initializeRequest(response: DebugProtocol.InitializeResponse, args: DebugProtocol.InitializeRequestArguments): void {
        response.body.supportsGotoTargetsRequest = true;
        response.body.supportsHitConditionalBreakpoints = true;
        response.body.supportsConfigurationDoneRequest = true;
        response.body.supportsConditionalBreakpoints = true;
        response.body.supportsFunctionBreakpoints = true;
        response.body.supportsEvaluateForHovers = true;
        response.body.supportsSetVariable = true;
        response.body.supportsLogPoints = true;

        if(process.arch=="arm64")
        {
            response.body.supportsStepBack = true;
        }

        response.body.supportsDisassembleRequest = true;
        response.body.supportsReadMemoryRequest = true;
        response.body.supportsInstructionBreakpoints = true;
        args.supportsRunInTerminalRequest = true;
        this.sendResponse(response);
    }

    // tslint:disable-next-line: max-line-length
    public sendErrorResponsePub(response: DebugProtocol.Response, codeOrMessage: number | DebugProtocol.Message, format?: string, variables?: any, dest?: any): void {
        this.sendErrorResponse(response, codeOrMessage, format, variables, dest);
    }

    protected disassembleRequest( response: DebugProtocol.DisassembleResponse, args: DebugProtocol.DisassembleArguments, request?: DebugProtocol.Request): void {
        this.disassember.disassembleProtocolRequest(response,args,request);
    }

    protected launchRequest(response: DebugProtocol.LaunchResponse, args: LaunchRequestArguments): void {
        const dbgCommand = args.gdbpath || "gdb";
        if (!this.checkCommand(dbgCommand)) {
            this.sendErrorResponse(response, 104, `Configured debugger ${dbgCommand} not found.`);
            return;
        }
        this.miDebugger = new MI2(dbgCommand, ["-q", "--interpreter=mi2"], args.debugger_args, args.env);

        this.args = args;
        this.disassember = new GdbDisassembler(this, args.showDevDebugOutput);
        this.setPathSubstitutions(args.pathSubstitutions);
        this.initDebugger();
        this.quit = false;
        this.attached = false;
        this.initialRunCommand = RunCommand.RUN;
        this.isSSH = false;
        this.started = false;
        this.crashed = false;
        this.setValuesFormattingMode(args.valuesFormatting);
        this.miDebugger.printCalls = !!args.printCalls;
        this.miDebugger.debugOutput = !!args.showDevDebugOutput;
        this.stopAtEntry = args.stopAtEntry;
        if (args.ssh !== undefined) {
            if (args.ssh.forwardX11 === undefined)
                args.ssh.forwardX11 = true;
            if (args.ssh.port === undefined)
                args.ssh.port = 22;
            if (args.ssh.x11port === undefined)
                args.ssh.x11port = 6000;
            if (args.ssh.x11host === undefined)
                args.ssh.x11host = "localhost";
            if (args.ssh.remotex11screen === undefined)
                args.ssh.remotex11screen = 0;
            this.isSSH = true;
            this.setSourceFileMap(args.ssh.sourceFileMap, args.ssh.cwd, args.cwd);
            this.miDebugger.ssh(args.ssh, args.ssh.cwd, args.target, args.arguments, args.terminal, false).then(() => {
                if (args.autorun)
                    args.autorun.forEach(command => {
                        this.miDebugger.sendUserInput(command);
                    });
                this.sendResponse(response);
            }, err => {
                this.sendErrorResponse(response, 102, `Failed to SSH: ${err.toString()}`);
            });
        } else {
            this.miDebugger.load(args.cwd, args.target, args.arguments, args.terminal).then(() => {
                if (args.autorun)
                    args.autorun.forEach(command => {
                        this.miDebugger.sendUserInput(command);
                    });
                    if(this.miDebugger.application.includes('gdb')){
                        if(args.terminal === 'integrated' || args.terminal === '' || args.terminal === undefined){
                            const terminalRequestArgs:DebugProtocol.RunInTerminalRequestArguments = {
                                kind: "integrated",
                                title: this.miDebugger.application,
                                cwd: args.cwd || '',
                                args: [],
                            };
                            if(process.platform != "win32"){
                                this.createIntegratedTerminalLinux(terminalRequestArgs);
                            }
                        }
                    }
                this.sendResponse(response);
            }, err => {
                this.sendErrorResponse(response, 103, `Failed to load MI Debugger: ${err.toString()}`);
            });
        }
    }

    protected attachRequest(response: DebugProtocol.AttachResponse, args: AttachRequestArguments): void {
        child_process.exec(`which ${args.gdbpath || "gdb"}`, (e,x,stderr)=>{
            if (e || !x || x.length == 0) {
                this.sendErrorResponse(response, 104, `No ${args.gdbpath || "gdb"} found, please install it.`);
                return
            }
        })
        this.miDebugger = new MI2(args.gdbpath || "gdb", ["-q", "--interpreter=mi2"], args.debugger_args, args.env);
        this.setPathSubstitutions(args.pathSubstitutions);
        this.initDebugger();
        this.quit = false;
        this.attached = !args.remote;
        this.initialRunCommand = !!args.stopAtConnect ? RunCommand.NONE : RunCommand.CONTINUE;
        this.isSSH = false;
        this.setValuesFormattingMode(args.valuesFormatting);
        this.miDebugger.printCalls = !!args.printCalls;
        this.miDebugger.debugOutput = !!args.showDevDebugOutput;
        this.stopAtEntry = args.stopAtEntry;
        this.args = args;
        this.disassember = new GdbDisassembler(this, args.showDevDebugOutput);
        if (args.ssh !== undefined) {
            if (args.ssh.forwardX11 === undefined)
                args.ssh.forwardX11 = true;
            if (args.ssh.port === undefined)
                args.ssh.port = 22;
            if (args.ssh.x11port === undefined)
                args.ssh.x11port = 6000;
            if (args.ssh.x11host === undefined)
                args.ssh.x11host = "localhost";
            if (args.ssh.remotex11screen === undefined)
                args.ssh.remotex11screen = 0;
            this.isSSH = true;
            this.setSourceFileMap(args.ssh.sourceFileMap, args.ssh.cwd, args.cwd);
            this.miDebugger.ssh(args.ssh, args.ssh.cwd, args.target, "", undefined, true).then(() => {
                if (args.autorun)
                    args.autorun.forEach(command => {
                        this.miDebugger.sendUserInput(command);
                    });
                this.sendResponse(response);
            }, err => {
                this.sendErrorResponse(response, 102, `Failed to SSH: ${err.toString()}`);
            });
        } else {
            if (args.remote) {
                this.miDebugger.connect(args.cwd, args.executable, args.target).then(() => {
                    if (args.autorun)
                        args.autorun.forEach(command => {
                            this.miDebugger.sendUserInput(command);
                        });
                    this.sendResponse(response);
                }, err => {
                    this.sendErrorResponse(response, 102, `Failed to attach: ${err.toString()}`);
                });
            } else {
                this.miDebugger.attach(args.cwd, args.executable, args.target).then(() => {
                    if (args.autorun)
                        args.autorun.forEach(command => {
                            this.miDebugger.sendUserInput(command);
                        });
                    this.sendResponse(response);
                }, err => {
                    this.sendErrorResponse(response, 101, `Failed to attach: ${err.toString()}`);
                });
            }
        }
    }

    // Add extra commands for source file path substitution in GDB-specific syntax
    protected setPathSubstitutions(substitutions: { [index: string]: string }): void {
        if (substitutions) {
            Object.keys(substitutions).forEach(source => {
                this.miDebugger.extraCommands.push("gdb-set substitute-path \"" + escape(source) + "\" \"" + escape(substitutions[source]) + "\"");
            })
        }
    }
}

DebugSession.run(GDBDebugSession);
