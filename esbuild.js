const esbuild = require('esbuild');
const fs = require('fs');
const path = require('path');

const production = process.argv.includes('--production');
const watch = process.argv.includes('--watch');

function fileChanged(srcPath, destPath) {
    const srcStat = fs.statSync(srcPath);
    const destStat = fs.statSync(destPath);
    // 比较最后修改时间
    return srcStat.mtime > destStat.mtime;
}

/**
 * @type {import('esbuild').Plugin}
 */
const esbuildProblemMatcherPlugin = {
    name: 'esbuild-problem-matcher',

    setup(build) {
        build.onStart(() => {
            console.log('[watch] build started');
        });
        build.onEnd(result => {
            result.errors.forEach(({ text, location }) => {
                console.error(`✘ [ERROR] ${text}`);
                console.error(`    ${location.file}:${location.line}:${location.column}:`);
            });
            console.log('[watch] build finished');
        });
    }
};

const extensionCtx = esbuild.context({
    entryPoints: ['src/frontend/extension.ts'],
    bundle: true,
    format: 'cjs',
    minify: production,
    sourcemap: !production,
    sourcesContent: false,
    platform: 'node',
    outfile: 'dist/extension.js',
    external: ['vscode'],
    logLevel: 'silent',
    plugins: [
        /* add to the end of plugins array */
        esbuildProblemMatcherPlugin,
    ]
});

const daCtx = esbuild.context({
    entryPoints: [
        'src/gdb.ts',
        'src/lldb.ts',
        'src/mago.ts',
    ],
    bundle: true,
    format: 'cjs',
    minify: production,
    sourcemap: !production,
    sourcesContent: false,
    platform: 'node',
    outdir: 'dist',
    logLevel: 'silent',
    plugins: [
        /* add to the end of plugins array */
        esbuildProblemMatcherPlugin
    ],
    external: ['*.node'],
});


async function main() {
    await fs.promises.rm('dist', { recursive: true, force: true });
    const ctxes = await Promise.all([extensionCtx, daCtx]);
    if (watch) {
        await Promise.all(ctxes.map(ctx => ctx.watch()));
    } else {
        await Promise.all(ctxes.map(ctx => ctx.rebuild()));
        await Promise.all(ctxes.map(ctx => ctx.dispose()));
    }
}

main().catch(e => {
    console.error(e);
    process.exit(1);
});